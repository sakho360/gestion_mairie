from openerp.osv import fields, osv
from tempfile import TemporaryFile
import base64
import xlwt
from os import path

class export_amount(osv.osv_memory):
    _name = 'export.amount'
    _description = 'generate calberson file'
    
    _columns = {
                    
                 'name' : fields.char('name', size=100,),
                 'path' : fields.char('Enregistrer sous', size=100, required=True),
                'periodicite':fields.many2one('account.journal', 'Type impot', domain="[('type','=','sale')]", required=True),

                    
                }   
    
    def export_fn(self, cr, user, ids, context={}):  
        
        tmpf = TemporaryFile('w+') 
        data = base64.encodestring(tmpf.read())
          
        partner_obj = self.pool.get("res.partner")
        wiz = self.browse(cr, user, ids[0], context)
        period = wiz.periodicite
        print period.name
        res = []
        if period.name == "Abonnement" :
            cr.execute("""select  distinct av.number, avl.id, avl.amount_original, rp.name 
                          from  account_voucher_line avl , res_partner rp , account_voucher av,account_journal aj
                          where    avl.voucher_id = av.id
                          and av.partner_id = rp.id     
                          and av.journal_impot = %s""", (period.id,))
            res = cr.fetchall() 
            
            account_voucher_line_ids = []
            for number, account_voucher_line_id, amount_original, partner_name  in res:
                account_voucher_line_ids.append(account_voucher_line_id)
                
            date_list = self.pool.get('account.voucher.line').read(cr, user, account_voucher_line_ids, ['date_original'], context=context)
            print date_list
            dict = {}
            for record in date_list:
                dict[str(record['id'])] = record['date_original']
            
            i = 1
            book = xlwt.Workbook(encoding="utf-8")
            sheet1 = book.add_sheet("Sheet 1")
            
            sheet1.write(0, 0, "number")
            sheet1.write(0, 1, "partner_name")
            sheet1.write(0, 2, "amount_original")
            sheet1.write(0, 3, "date")
            book.save(wiz.path + "/Abonnement.xls")
            for number, account_voucher_line_id, amount_original, partner_name  in res:
                sheet1.write(i, 0, number)
                sheet1.write(i, 1, partner_name)
                sheet1.write(i, 2, amount_original)
                sheet1.write(i, 3, dict[str(account_voucher_line_id)])
                book.save(wiz.path + "/Abonnement.xls")
                i = i + 1
          

        elif period.name == "Diouti" :
            cr.execute("""select  distinct av.number, avl.id, avl.amount_original, rp.name , zone.name
                          from  account_voucher_line avl , res_partner rp , account_voucher av,account_journal aj,mairie_zone zone
                          where    avl.voucher_id = av.id
                          and rp.zone_id = zone.id
                          and av.partner_id = rp.id     
                          and av.journal_impot = %s""", (period.id,))
            res = cr.fetchall() 
            
            account_voucher_line_ids = []
            for number, account_voucher_line_id, amount_original, partner_name , zone in res:
                account_voucher_line_ids.append(account_voucher_line_id)
                
            date_list = self.pool.get('account.voucher.line').read(cr, user, account_voucher_line_ids, ['date_original'], context=context)
            print date_list
            dict = {}
            for record in date_list:
                dict[str(record['id'])] = record['date_original']
            
            i = 1
            book = xlwt.Workbook(encoding="utf-8")
            sheet1 = book.add_sheet("Sheet 1")
            
            sheet1.write(0, 0, "number")
            sheet1.write(0, 1, "partner_name")
            sheet1.write(0, 2, "zone")
            sheet1.write(0, 3, "amount_original")
            sheet1.write(0, 4, "date")
            book.save(wiz.path + "/Diouti.xls")
            for number, account_voucher_line_id, amount_original, partner_name , zone in res:
                sheet1.write(i, 0, number)
                sheet1.write(i, 1, partner_name)
                sheet1.write(i, 2, zone)
                sheet1.write(i, 3, amount_original)
                sheet1.write(i, 4, dict[str(account_voucher_line_id)])
                book.save(wiz.path + "/Diouti.xls")
                i = i + 1
          
        elif period.name == "Patente" :
            cr.execute("""select  distinct av.number, avl.id, avl.amount_original, rp.name 
                          from  account_voucher_line avl , res_partner rp , account_voucher av,account_journal aj
                          where    avl.voucher_id = av.id
                          and av.partner_id = rp.id     
                          and av.journal_impot = %s""", (period.id,))
            res = cr.fetchall() 
            
            account_voucher_line_ids = []
            for number, account_voucher_line_id, amount_original, partner_name  in res:
                account_voucher_line_ids.append(account_voucher_line_id)
                
            date_list = self.pool.get('account.voucher.line').read(cr, user, account_voucher_line_ids, ['date_original'], context=context)
            print date_list
            dict = {}
            for record in date_list:
                dict[str(record['id'])] = record['date_original']
            
            i = 1
            book = xlwt.Workbook(encoding="utf-8")
            sheet1 = book.add_sheet("Sheet 1")
            
            sheet1.write(0, 0, "number")
            sheet1.write(0, 1, "partner_name")
            sheet1.write(0, 2, "amount_original")
            sheet1.write(0, 3, "date")
            book.save(wiz.path + "/Patente.xls")
            for number, account_voucher_line_id, amount_original, partner_name  in res:
                sheet1.write(i, 0, number)
                sheet1.write(i, 1, partner_name)
                sheet1.write(i, 2, amount_original)
                sheet1.write(i, 3, dict[str(account_voucher_line_id)])
                book.save(wiz.path + "/Patente.xls")
                i = i + 1
        line = "bonjour mido"
        tmpf.write(line)  
        tmpf.seek(0)
        data = base64.encodestring(tmpf.read())
            
    
        return {}  
    
export_amount()

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:

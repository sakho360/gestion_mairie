# -*- coding: utf-8 -*-
from openerp.osv import osv, fields
from operator import itemgetter
import time
from openerp.osv.fields import many2one
from openerp.tools.translate import _
from openerp import api
import datetime
from dateutil.relativedelta import relativedelta
from openerp import netsvc
import openerp.addons.decimal_precision as dp

   
class secteur(osv.osv):
    _name = 'mairie.secteur'
    _description = ' Secteur'    
    _columns = {
                'name':fields.char('Secteur', size=64, required=True),
                'user_id':fields.many2one('res.users', 'Responsable', required=True)                
                }
secteur()


class zone(osv.osv):
    _name = 'mairie.zone'
    _description = ' Zone'    
    _columns = {
                'name':fields.char('Zone', size=64, required=True),
                'diouti':fields.float('Diouti', required=True)                
                }
zone()


class type_impot(osv.osv):
    
    _name = 'gm.type.impot'
    
  
    _description = 'Type Impot'
    _sql_constraints = [('name_uniq', 'unique (name)', 'Impot doit etre unique !'), ]   
    _columns = {
        'taxe_id':fields.many2one('account.tax', 'Taxe'),
        'name':fields.char('Impot', size=64, required=True,),
        'frequence':fields.selection([
                    ('journaliere', 'Journaliere'),
                    ('hebdomadaire', 'Hebdomadaire'),
                    ('mensuelle', 'Mensuelle'),
                    ('trimestrielle', 'Trimestrielle'),
                     ('semestrielle', 'Semestrielle'),
                    ('annuelle', 'Annuelle')
                     ], 'Frequence', select=True, require=True),
 }
    
    
   
type_impot()

class impot(osv.osv):
    
    _name = 'gm.impot'
    
  
    _description = 'Impot'
    
    _defaults = {
            'patente':0,
               }
    
    _columns = {
        'patente':fields.integer('Montant', required="True"),
        'partner_id':fields.many2one('res.partner', 'commerçant'),
        'periodicite':fields.many2one('account.journal', 'Type impot', domain="[('type','=','sale')]", required=True),
        'existe_dec':fields.boolean('Declaration genere'),
#         'property_payment_term': fields.(
#             'account.payment.term',
#             type='many2one',
#             relation='account.payment.term',
#             string ='Customer Payment Term',
#             view_load=True,),

#             help="This payment term will be used instead of the default one for sale orders and customer invoices"
         'property_payment_term': fields.many2one('account.payment.term', string='Customer Payment Term'),
#             view_load=True,),


              }
    
    
   
impot()

class partner(osv.osv):
    
    _name = 'res.partner'
    _inherit = 'res.partner'
    
    
    
    def onchange_section_id(self, cr, uid, ids, section_id, context=None):
        # TODO : compute new values from the db/system
        result = {}
        section = self.pool.get("mairie.secteur").browse(cr, uid, section_id, context)
        result['value'] = {'user_id':section.user_id.id}
        return result


#   =======================================    abonnemen      =========================================================
# customer  agent
    def _get_amounts_abonnement_and_date(self, cr, uid, ids, name, arg, context=None):

            
      
        res = {}
        company = self.pool.get('res.users').browse(cr, uid, uid, context=context).company_id
        current_date = fields.date.context_today(self, cr, uid, context=context)
        for partner in self.browse(cr, uid, ids, context=context):
            worst_due_date = False
            amount_due = amount_overdue = 0.0
            for aml in partner.unreconciled_aml_ids:
                journal_ids = self.pool.get('account.journal').search(cr, uid, [('name', '=', 'Abonnement')], context=context)
                if (aml.company_id == company) and (aml.journal_id.id == journal_ids[0]):
                        date_maturity = aml.date_maturity or aml.date
                        if not worst_due_date or date_maturity < worst_due_date:
                            worst_due_date = date_maturity
                        amount_due += aml.result
                        if (date_maturity <= current_date):
                            amount_overdue += aml.result
            res[partner.id] = amount_due
                               
        return res

    def _get_payment_abonnement_amount_overdue(self, cr, uid, ids, name, arg, context=None):
       
        res = {}
        company = self.pool.get('res.users').browse(cr, uid, uid, context=context).company_id
        current_date = fields.date.context_today(self, cr, uid, context=context)
        for partner in self.browse(cr, uid, ids, context=context):
            worst_due_date = False
            amount_due = amount_overdue = 0.0
            for aml in partner.unreconciled_aml_ids:
                journal_ids = self.pool.get('account.journal').search(cr, uid, [('name', '=', 'Abonnement')], context=context)
                if (aml.company_id == company) and (aml.journal_id.id == journal_ids[0]):
                        date_maturity = aml.date_maturity or aml.date
                        if not worst_due_date or date_maturity < worst_due_date:
                            worst_due_date = date_maturity
                        amount_due += aml.result
                        if (date_maturity <= current_date):
                            amount_overdue += aml.result
            res[partner.id] = amount_overdue
                                
        return res

    def _get_payment_abonnement_earliest_due_date(self, cr, uid, ids, name, arg, context=None):
       
        res = {}
        company = self.pool.get('res.users').browse(cr, uid, uid, context=context).company_id
        current_date = fields.date.context_today(self, cr, uid, context=context)
        for partner in self.browse(cr, uid, ids, context=context):
            worst_due_date = False
            amount_due = amount_overdue = 0.0
            for aml in partner.unreconciled_aml_ids:
                journal_ids = self.pool.get('account.journal').search(cr, uid, [('name', '=', 'Abonnement')], context=context)
                if (aml.company_id == company) and (aml.journal_id.id == journal_ids[0]):
                        date_maturity = aml.date_maturity or aml.date
                        if not worst_due_date or date_maturity < worst_due_date:
                            worst_due_date = date_maturity
                        amount_due += aml.result
                        if (date_maturity <= current_date):
                            amount_overdue += aml.result
            res[partner.id] = worst_due_date
                                
        return res
    
    def _payment_abonnement_due_search(self, cr, uid, obj, name, args, context=None):
        if not args:
            return []
        query, query_args = self._get_followup_overdue_query(cr, uid, args, overdue_only=False, context=context)
        cr.execute(query, query_args)
        res = cr.fetchall()
        if not res:
            return [('id', '=', '0')]
        partner_ids = [x[0] for x in res]
        list_partner_brows = self.pool.get('res.partner').browse(cr, uid, partner_ids, context=None)
        result = [('id', 'in', [partner.id for partner in list_partner_brows if partner.payment_abonnement_due > 0])]
        return result
     
    def _payment_abonnement_overdue_search(self, cr, uid, obj, name, args, context=None):
        if not args:
            return []
        query, query_args = self._get_followup_overdue_query(cr, uid, args, overdue_only=True, context=context)
        cr.execute(query, query_args)
        res = cr.fetchall()
        if not res:
            return [('id', '=', '0')]
#        return [('id','in', [x[0] for x in res])]
        partner_ids = [x[0] for x in res]
        list_partner_brows = self.pool.get('res.partner').browse(cr, uid, partner_ids, context=None)
        result = [('id', 'in', [partner.id for partner in list_partner_brows if partner.payment_abonnement_due > 0])]
        return result

    def _payment_abonnement_earliest_date_search(self, cr, uid, obj, name, args, context=None):
        if not args:
            return []
        company_id = self.pool.get('res.users').browse(cr, uid, uid, context=context).company_id.id
        having_where_clause = ' AND '.join(map(lambda x: '(MIN(l.date_maturity) %s %%s)' % (x[1]), args))
        having_values = [x[2] for x in args]
        query = self.pool.get('account.move.line')._query_get(cr, uid, context=context)
        cr.execute('SELECT partner_id FROM account_move_line l '\
                    'WHERE account_id IN '\
                        '(SELECT id FROM account_account '\
                        'WHERE type=\'receivable\' AND active) '\
                    'AND l.company_id = %s '
                    'AND reconcile_id IS NULL '\
                    'AND ' + query + ' '\
                    'AND partner_id IS NOT NULL '\
                    'GROUP BY partner_id HAVING ' + having_where_clause,
                     [company_id] + having_values)
        res = cr.fetchall()
        if not res:
            return [('id', '=', '0')]
#        return [('id','in', [x[0] for x in res])]
        partner_ids = [x[0] for x in res]
        list_partner_brows = self.pool.get('res.partner').browse(cr, uid, partner_ids, context=None)
        result = [('id', 'in', [partner.id for partner in list_partner_brows if partner.payment_abonnement_due > 0])]
        return result



#   =======================================     diouti     =========================================================
# customer  agent
    
    def _get_amounts_diouti_and_date(self, cr, uid, ids, name, arg, context=None):
       
        res = {}
        company = self.pool.get('res.users').browse(cr, uid, uid, context=context).company_id
        current_date = fields.date.context_today(self, cr, uid, context=context)
        for partner in self.browse(cr, uid, ids, context=context):
            worst_due_date = False
            amount_due = amount_overdue = 0.0
            for aml in partner.unreconciled_aml_ids:
                journal_ids = self.pool.get('account.journal').search(cr, uid, [('name', '=', 'Diouti')], context=context)
                if (aml.company_id == company) and (aml.journal_id.id == journal_ids[0]):
                        date_maturity = aml.date_maturity or aml.date
                        if not worst_due_date or date_maturity < worst_due_date:
                            worst_due_date = date_maturity
                        amount_due += aml.result
                        if (date_maturity <= current_date):
                            amount_overdue += aml.result
            res[partner.id] = amount_due
                                
        return res
 
    def _get_payment_diouti_amount_overdue(self, cr, uid, ids, name, arg, context=None):
       
        res = {}
        company = self.pool.get('res.users').browse(cr, uid, uid, context=context).company_id
        current_date = fields.date.context_today(self, cr, uid, context=context)
        for partner in self.browse(cr, uid, ids, context=context):
            worst_due_date = False
            amount_due = amount_overdue = 0.0
            for aml in partner.unreconciled_aml_ids:
                journal_ids = self.pool.get('account.journal').search(cr, uid, [('name', '=', 'Diouti')], context=context)
                if (aml.company_id == company) and (aml.journal_id.id == journal_ids[0]):
                        date_maturity = aml.date_maturity or aml.date
                        if not worst_due_date or date_maturity < worst_due_date:
                            worst_due_date = date_maturity
                        amount_due += aml.result
                        if (date_maturity <= current_date):
                            amount_overdue += aml.result
            res[partner.id] = amount_overdue
                                
        return res

    def _get_payment_diouti_earliest_due_date(self, cr, uid, ids, name, arg, context=None):
       
        res = {}
        company = self.pool.get('res.users').browse(cr, uid, uid, context=context).company_id
        current_date = fields.date.context_today(self, cr, uid, context=context)
        for partner in self.browse(cr, uid, ids, context=context):
            worst_due_date = False
            amount_due = amount_overdue = 0.0
            for aml in partner.unreconciled_aml_ids:
                journal_ids = self.pool.get('account.journal').search(cr, uid, [('name', '=', 'Diouti')], context=context)
                if (aml.company_id == company) and (aml.journal_id.id == journal_ids[0]):
                        date_maturity = aml.date_maturity or aml.date
                        if not worst_due_date or date_maturity < worst_due_date:
                            worst_due_date = date_maturity
                        amount_due += aml.result
                        if (date_maturity <= current_date):
                            amount_overdue += aml.result
            res[partner.id] = worst_due_date
                                
        return res

    def _payment_diouti_due_search(self, cr, uid, obj, name, args, context=None):
        if not args:
            return []
        query, query_args = self._get_followup_overdue_query(cr, uid, args, overdue_only=False, context=context)
        cr.execute(query, query_args)
        res = cr.fetchall()
        if not res:
            return [('id', '=', '0')]
        partner_ids = [x[0] for x in res]
        list_partner_brows = self.pool.get('res.partner').browse(cr, uid, partner_ids, context=None)
        result = [('id', 'in', [partner.id for partner in list_partner_brows if partner.payment_diouti_due > 0])]
        return result

    def _payment_diouti_overdue_search(self, cr, uid, obj, name, args, context=None):
        if not args:
            return []
        query, query_args = self._get_followup_overdue_query(cr, uid, args, overdue_only=True, context=context)
        cr.execute(query, query_args)
        res = cr.fetchall()
        if not res:
            return [('id', '=', '0')]
#        return [('id','in', [x[0] for x in res])]
        partner_ids = [x[0] for x in res]
        list_partner_brows = self.pool.get('res.partner').browse(cr, uid, partner_ids, context=None)
        result = [('id', 'in', [partner.id for partner in list_partner_brows if partner.payment_diouti_due > 0])]
        return result

    def _payment_diouti_earliest_date_search(self, cr, uid, obj, name, args, context=None):
        if not args:
            return []
        company_id = self.pool.get('res.users').browse(cr, uid, uid, context=context).company_id.id
        having_where_clause = ' AND '.join(map(lambda x: '(MIN(l.date_maturity) %s %%s)' % (x[1]), args))
        having_values = [x[2] for x in args]
        query = self.pool.get('account.move.line')._query_get(cr, uid, context=context)
        cr.execute('SELECT partner_id FROM account_move_line l '\
                    'WHERE account_id IN '\
                        '(SELECT id FROM account_account '\
                        'WHERE type=\'receivable\' AND active) '\
                    'AND l.company_id = %s '
                    'AND reconcile_id IS NULL '\
                    'AND ' + query + ' '\
                    'AND partner_id IS NOT NULL '\
                    'GROUP BY partner_id HAVING ' + having_where_clause,
                     [company_id] + having_values)
        res = cr.fetchall()
        if not res:
            return [('id', '=', '0')]
#        return [('id','in', [x[0] for x in res])]
        partner_ids = [x[0] for x in res]
        list_partner_brows = self.pool.get('res.partner').browse(cr, uid, partner_ids, context=None)
        result = [('id', 'in', [partner.id for partner in list_partner_brows if partner.payment_diouti_due > 0])]
        return result


#   =======================================    patente      =========================================================
# customer  agent
             

    def _get_amounts_patente_and_date(self, cr, uid, ids, name, arg, context=None):
      
        res = {}
        company = self.pool.get('res.users').browse(cr, uid, uid, context=context).company_id
        current_date = fields.date.context_today(self, cr, uid, context=context)
        for partner in self.browse(cr, uid, ids, context=context):
            worst_due_date = False
            amount_due = amount_overdue = 0.0
            for aml in partner.unreconciled_aml_ids:
                journal_ids = self.pool.get('account.journal').search(cr, uid, [('name', '=', 'Patente')], context=context)
                if (aml.company_id == company) and (aml.journal_id.id == journal_ids[0]):
                        date_maturity = aml.date_maturity or aml.date
                        if not worst_due_date or date_maturity < worst_due_date:
                            worst_due_date = date_maturity
                        amount_due += aml.result
                        if (date_maturity <= current_date):
                            amount_overdue += aml.result
            res[partner.id] = amount_due
                               
        return res

    def _get_payment_patente_amount_overdue(self, cr, uid, ids, name, arg, context=None):
       
        res = {}
        company = self.pool.get('res.users').browse(cr, uid, uid, context=context).company_id
        current_date = fields.date.context_today(self, cr, uid, context=context)
        for partner in self.browse(cr, uid, ids, context=context):
            worst_due_date = False
            amount_due = amount_overdue = 0.0
            for aml in partner.unreconciled_aml_ids:
                journal_ids = self.pool.get('account.journal').search(cr, uid, [('name', '=', 'Patente')], context=context)
                if (aml.company_id == company) and (aml.journal_id.id == journal_ids[0]):
                        date_maturity = aml.date_maturity or aml.date
                        if not worst_due_date or date_maturity < worst_due_date:
                            worst_due_date = date_maturity
                        amount_due += aml.result
                        if (date_maturity <= current_date):
                            amount_overdue += aml.result
            res[partner.id] = amount_overdue
                                
        return res

    def _get_payment_patente_earliest_due_date(self, cr, uid, ids, name, arg, context=None):
       
        res = {}
        company = self.pool.get('res.users').browse(cr, uid, uid, context=context).company_id
        current_date = fields.date.context_today(self, cr, uid, context=context)
        for partner in self.browse(cr, uid, ids, context=context):
            worst_due_date = False
            amount_due = amount_overdue = 0.0
            for aml in partner.unreconciled_aml_ids:
                journal_ids = self.pool.get('account.journal').search(cr, uid, [('name', '=', 'Patente')], context=context)
                if (aml.company_id == company) and (aml.journal_id.id == journal_ids[0]):
                        date_maturity = aml.date_maturity or aml.date
                        if not worst_due_date or date_maturity < worst_due_date:
                            worst_due_date = date_maturity
                        amount_due += aml.result
                        if (date_maturity <= current_date):
                            amount_overdue += aml.result
            res[partner.id] = worst_due_date
                                
        return res
          
    def _payment_patente_due_search(self, cr, uid, obj, name, args, context=None):
        if not args:
            return []
        query, query_args = self._get_followup_overdue_query(cr, uid, args, overdue_only=False, context=context)
        cr.execute(query, query_args)
        res = cr.fetchall()
        if not res:
            return [('id', '=', '0')]
        partner_ids = [x[0] for x in res]
        list_partner_brows = self.pool.get('res.partner').browse(cr, uid, partner_ids, context=None)
        result = [('id', 'in', [partner.id for partner in list_partner_brows if partner.payment_patente_due > 0])]
        return result

    def _payment_patente_overdue_search(self, cr, uid, obj, name, args, context=None):
        if not args:
            return []
        query, query_args = self._get_followup_overdue_query(cr, uid, args, overdue_only=True, context=context)
        cr.execute(query, query_args)
        res = cr.fetchall()
        if not res:
            return [('id', '=', '0')]
#        return [('id','in', [x[0] for x in res])]
        partner_ids = [x[0] for x in res]
        list_partner_brows = self.pool.get('res.partner').browse(cr, uid, partner_ids, context=None)
        result = [('id', 'in', [partner.id for partner in list_partner_brows if partner.payment_patente_due > 0])]
        return result

    def _payment_patente_earliest_date_search(self, cr, uid, obj, name, args, context=None):
        if not args:
            return []
        company_id = self.pool.get('res.users').browse(cr, uid, uid, context=context).company_id.id
        having_where_clause = ' AND '.join(map(lambda x: '(MIN(l.date_maturity) %s %%s)' % (x[1]), args))
        having_values = [x[2] for x in args]
        query = self.pool.get('account.move.line')._query_get(cr, uid, context=context)
        cr.execute('SELECT partner_id FROM account_move_line l '\
                    'WHERE account_id IN '\
                        '(SELECT id FROM account_account '\
                        'WHERE type=\'receivable\' AND active) '\
                    'AND l.company_id = %s '
                    'AND reconcile_id IS NULL '\
                    'AND ' + query + ' '\
                    'AND partner_id IS NOT NULL '\
                    'GROUP BY partner_id HAVING ' + having_where_clause,
                     [company_id] + having_values)
        res = cr.fetchall()
        if not res:
            return [('id', '=', '0')]
#        return [('id','in', [x[0] for x in res])]
        partner_ids = [x[0] for x in res]
        list_partner_brows = self.pool.get('res.partner').browse(cr, uid, partner_ids, context=None)
        result = [('id', 'in', [partner.id for partner in list_partner_brows if partner.payment_patente_due > 0])]
        return result

#   =======================================    Tax sur publicité      =========================================================
# customer  commerçant
             

    def _get_amounts_tax_pub_and_date(self, cr, uid, ids, name, arg, context=None):
      
        res = {}
        company = self.pool.get('res.users').browse(cr, uid, uid, context=context).company_id
        current_date = fields.date.context_today(self, cr, uid, context=context)
        for partner in self.browse(cr, uid, ids, context=context):
            worst_due_date = False
            amount_due = amount_overdue = 0.0
            for aml in partner.unreconciled_aml_ids:
                journal_ids = self.pool.get('account.journal').search(cr, uid, [('code', '=', 'TAXPU')], context=context)
                if (aml.company_id == company) and (aml.journal_id.id == journal_ids[0]):
                        date_maturity = aml.date_maturity or aml.date
                        if not worst_due_date or date_maturity < worst_due_date:
                            worst_due_date = date_maturity
                        amount_due += aml.result
                        if (date_maturity <= current_date):
                            amount_overdue += aml.result
            res[partner.id] = amount_due
                               
        return res

    def _get_payment_tax_pub_amount_overdue(self, cr, uid, ids, name, arg, context=None):
       
        res = {}
        company = self.pool.get('res.users').browse(cr, uid, uid, context=context).company_id
        current_date = fields.date.context_today(self, cr, uid, context=context)
        for partner in self.browse(cr, uid, ids, context=context):
            worst_due_date = False
            amount_due = amount_overdue = 0.0
            for aml in partner.unreconciled_aml_ids:
                journal_ids = self.pool.get('account.journal').search(cr, uid, [('code', '=', 'TAXPU')], context=context)
                if (aml.company_id == company) and (aml.journal_id.id == journal_ids[0]):
                        date_maturity = aml.date_maturity or aml.date
                        if not worst_due_date or date_maturity < worst_due_date:
                            worst_due_date = date_maturity
                        amount_due += aml.result
                        if (date_maturity <= current_date):
                            amount_overdue += aml.result
            res[partner.id] = amount_overdue
                                
        return res

    def _get_payment_tax_pub_earliest_due_date(self, cr, uid, ids, name, arg, context=None):
       
        res = {}
        company = self.pool.get('res.users').browse(cr, uid, uid, context=context).company_id
        current_date = fields.date.context_today(self, cr, uid, context=context)
        for partner in self.browse(cr, uid, ids, context=context):
            worst_due_date = False
            amount_due = amount_overdue = 0.0
            for aml in partner.unreconciled_aml_ids:
                journal_ids = self.pool.get('account.journal').search(cr, uid, [('code', '=', 'TAXPU')], context=context)
                if (aml.company_id == company) and (aml.journal_id.id == journal_ids[0]):
                        date_maturity = aml.date_maturity or aml.date
                        if not worst_due_date or date_maturity < worst_due_date:
                            worst_due_date = date_maturity
                        amount_due += aml.result
                        if (date_maturity <= current_date):
                            amount_overdue += aml.result
            res[partner.id] = worst_due_date
                                
        return res
          
    def _payment_tax_pub_due_search(self, cr, uid, obj, name, args, context=None):
        if not args:
            return []
        query, query_args = self._get_followup_overdue_query(cr, uid, args, overdue_only=False, context=context)
        cr.execute(query, query_args)
        res = cr.fetchall()
        if not res:
            return [('id', '=', '0')]
        partner_ids = [x[0] for x in res]
        list_partner_brows = self.pool.get('res.partner').browse(cr, uid, partner_ids, context=None)
        result = [('id', 'in', [partner.id for partner in list_partner_brows if partner.payment_tax_pub_due > 0])]
        return result

    def _payment_tax_pub_overdue_search(self, cr, uid, obj, name, args, context=None):
        if not args:
            return []
        query, query_args = self._get_followup_overdue_query(cr, uid, args, overdue_only=True, context=context)
        cr.execute(query, query_args)
        res = cr.fetchall()
        if not res:
            return [('id', '=', '0')]
#        return [('id','in', [x[0] for x in res])]
        partner_ids = [x[0] for x in res]
        list_partner_brows = self.pool.get('res.partner').browse(cr, uid, partner_ids, context=None)
        result = [('id', 'in', [partner.id for partner in list_partner_brows if partner.payment_tax_pub_due > 0])]
        return result

    def _payment_tax_pub_earliest_date_search(self, cr, uid, obj, name, args, context=None):
        if not args:
            return []
        company_id = self.pool.get('res.users').browse(cr, uid, uid, context=context).company_id.id
        having_where_clause = ' AND '.join(map(lambda x: '(MIN(l.date_maturity) %s %%s)' % (x[1]), args))
        having_values = [x[2] for x in args]
        query = self.pool.get('account.move.line')._query_get(cr, uid, context=context)
        cr.execute('SELECT partner_id FROM account_move_line l '\
                    'WHERE account_id IN '\
                        '(SELECT id FROM account_account '\
                        'WHERE type=\'receivable\' AND active) '\
                    'AND l.company_id = %s '
                    'AND reconcile_id IS NULL '\
                    'AND ' + query + ' '\
                    'AND partner_id IS NOT NULL '\
                    'GROUP BY partner_id HAVING ' + having_where_clause,
                     [company_id] + having_values)
        res = cr.fetchall()
        if not res:
            return [('id', '=', '0')]
#        return [('id','in', [x[0] for x in res])]
        partner_ids = [x[0] for x in res]
        list_partner_brows = self.pool.get('res.partner').browse(cr, uid, partner_ids, context=None)
        result = [('id', 'in', [partner.id for partner in list_partner_brows if partner.payment_tax_pub_due > 0])]
        return result


#   ==========================================================================================================

    def _hav_abonnement_fn(self, cursor, user, ids, name, arg, context=None):

        res = {}
        for partner in self.browse(cursor, user, ids, context=context):
            res[partner.id] = True
#             invoice_existence = False
            for journal in sale.impot_ids.periodicite.name:
                if journal != 'Abonnement':
                    res[partner.id] = False
            
        return res
        
    def onchange_zone_id(self, cr, uid, ids, zone_id, context=None):
        zone_obj = self.pool.get('mairie.zone')
        zone = zone_obj.browse(cr, uid, zone_id, context=context)
        if zone_id:
               return {'value':{'montant_diouti':zone.diouti}}
        return {}
   
    def _get_abonnement_list(self, cr, uid, ids, name, arg, context=None): 
        res = {}
        obj = self.pool.get('account.analytic.account')
        for id in ids:
            res[id] = obj.search(cr, uid, [('partner_id', '=', id), ('type', '=', 'contract'), ('recurring_rule_type', '=', 'monthly')])
        return res
    
    def _get_patente_list(self, cr, uid, ids, name, arg, context=None): 
        res = {}
        obj = self.pool.get('account.analytic.account')
        for id in ids:
            res[id] = obj.search(cr, uid, [('partner_id', '=', id), ('type', '=', 'contract'), ('recurring_rule_type', '=', 'yearly'),('is_patente','=',True)])
        return res 

    def _get_tax_pub_list(self, cr, uid, ids, name, arg, context=None): 
        res = {}
        obj = self.pool.get('account.analytic.account')
        for id in ids:
            res[id] = obj.search(cr, uid, [('partner_id', '=', id), ('type', '=', 'contract'), ('recurring_rule_type', '=', 'yearly'),('is_patente','=',False)])
        return res 
          
    _description = ' les différents types des contracts de la mairie: abonnement, diouti, patente et taxe sur publicité'
    
    _defaults = {
             
               }
     
    _columns = {
          
#                 =============================   Abonnement   ===================================
        'payment_abonnement_due':fields.function(_get_amounts_abonnement_and_date, type='float', string="Montant d'Abonnement du",
                                                 fnct_search=_payment_abonnement_due_search, digits_compute=dp.get_precision('Account')),
        
        'payment_abonnement_amount_overdue':fields.function(_get_payment_abonnement_amount_overdue,
                                                 type='float', string="Montant d'Abonnement en retard",
                                                 fnct_search=_payment_abonnement_overdue_search, digits_compute=dp.get_precision('Account')),
                
        'payment_abonnement_earliest_due_date':fields.function(_get_payment_abonnement_earliest_due_date,
                                                    type='date',
                                                    string="Echeance d'Abonnement la plus en retard ",
                                                    fnct_search=_payment_abonnement_earliest_date_search, digits_compute=dp.get_precision('Account')),
#                 ===========================  diouti ============================
        'payment_diouti_due':fields.function(_get_amounts_diouti_and_date, type='float', string="Montant Diouti Du",
                                                 fnct_search=_payment_diouti_due_search, digits_compute=dp.get_precision('Account')),

        'payment_diouti_amount_overdue':fields.function(_get_payment_diouti_amount_overdue,
                                                 type='float', string="Montant Diouti en retard",
                                                 fnct_search=_payment_diouti_overdue_search, digits_compute=dp.get_precision('Account')),
                
        'payment_diouti_earliest_due_date':fields.function(_get_payment_diouti_earliest_due_date,
                                                    type='date',
                                                    string="Echeance de Diouti la plus en retard",
                                                    fnct_search=_payment_diouti_earliest_date_search, digits_compute=dp.get_precision('Account')),
#                 ==============================  patente ========================================
        'payment_patente_due':fields.function(_get_amounts_patente_and_date, type='float', string="Montant de Patente du",
                                                 fnct_search=_payment_patente_due_search, digits_compute=dp.get_precision('Account')),
        
        'payment_patente_amount_overdue':fields.function(_get_payment_patente_amount_overdue,
                                                 type='float', string="Montant de Patente en retard",
                                                 fnct_search=_payment_patente_overdue_search, digits_compute=dp.get_precision('Account')),
                
        'payment_patente_earliest_due_date':fields.function(_get_payment_patente_earliest_due_date,
                                                    type='date',
                                                    string="Echeance de Patente la plus en retard",
                                                    fnct_search=_payment_patente_earliest_date_search, digits_compute=dp.get_precision('Account')),
#                 ==============================  Tax sur publicité ========================================
        'payment_tax_pub_due':fields.function(_get_amounts_tax_pub_and_date, type='float', string="Montant de tax sur publicité du",
                                                 fnct_search=_payment_tax_pub_due_search, digits_compute=dp.get_precision('Account')),
        
        'payment_tax_pub_amount_overdue':fields.function(_get_payment_tax_pub_amount_overdue,
                                                 type='float', string="Montant de tax sur publicité en retard",
                                                 fnct_search=_payment_tax_pub_overdue_search, digits_compute=dp.get_precision('Account')),
                
        'payment_tax_pub_earliest_due_date':fields.function(_get_payment_tax_pub_earliest_due_date,
                                                    type='date',
                                                    string="Echeance de tax sur publicité la plus en retard",
                                                    fnct_search=_payment_tax_pub_earliest_date_search, digits_compute=dp.get_precision('Account')),
#                       ==============================================================================================          
        'section_id':fields.many2one('mairie.secteur', "Secteur"),
        'impot_ids':fields.one2many('gm.impot', 'partner_id', 'Impots', required=False),
        'agent': fields.boolean('Agent', required=False),
        'commercant': fields.boolean('Commerçant', required=False),
        'is_preseption': fields.boolean('preseption', required=False),
        'zone_id':fields.many2one('mairie.zone', 'Zone de recouvrement', required=False),
        # 'abonnement_ids' : fields.one2many('account.analytic.account', 'partner_id', string='Abonnements' ),
        'abonnement_idss' : fields.function(_get_abonnement_list, type='one2many', obj='account.analytic.account', string="Abonnements"),
        'patente_idss' : fields.function(_get_patente_list, type='one2many', obj='account.analytic.account', string="Patentes"),
        'patente_ids' : fields.one2many('account.analytic.account', 'partner_id', string='Patentes'),
        'tax_pub_idss' : fields.function(_get_tax_pub_list, type='one2many', obj='account.analytic.account', string="Taxes sur publicités"),
        'tax_pub_ids' : fields.one2many('account.analytic.account', 'partner_id', string='Taxes sur publicités'),
        'matricule':  fields.char('Matricule'),
        'montant_diouti':  fields.float('Montant diouti', digits_compute=dp.get_precision('Account')),
      
        
               }
    _defaults = {
             'is_preseption': False
                }
    _sql_constraints = [('matricule_uniq', 'unique (matricule)', 'Matricule doit etre unique !'), ]   
  
partner()

class account_invoice(osv.osv):
    
    _inherit = "account.invoice"
   
#     def onchange_partner_id(self, cr, uid, ids, type, partner_id,\
#                 date_invoice=False, payment_term=False, partner_bank_id=False, company_id=False):
#         result = super(account_invoice, self).onchange_partner_id(cr, uid, ids, type, partner_id,\
#                 date_invoice, payment_term, partner_bank_id, company_id)
#         obj = self.pool.get('res.partner').browse(cr, uid, partner_id, context=None)
#         if obj:
#             result['value']['credit'] = obj.payment_amount_overdue
#              
#         return result
    @api.multi
    def invoice_print1(self):
        """ Print the invoice and mark it as sent, so that we can see more
            easily the next step of the workflow
        """
        assert len(self) == 1, 'This option should only be used for a single id at a time.'
        self.sent = True
        return self.env['report'].get_action(self, 'gestion_mairie.report_fact')

    def calculate_date(self, cr, uid, ids, fields_name, args, context):
        res={} 
        invoice_obj = self.pool.get('account.invoice')
        for id in ids :
            if(invoice_obj.browse(cr, uid, id, context).date_due==False):
                next_date=time.strftime('%Y-%m-%d')
            else:
                next_date = datetime.datetime.strptime(invoice_obj.browse(cr, uid, id, context).date_due, "%Y-%m-%d")+ relativedelta(days=15)
            res[id]=next_date
        return res

    def invoice_pay_customer(self, cr, uid, ids, context=None):
        if not ids: return []
        dummy, view_id = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'gestion_mairie', 'view_vendor_receipt_dialog_form')

        inv = self.browse(cr, uid, ids[0], context=context)
        return {
            'name':_("Pay Invoice"),
            'view_mode': 'form',
            'view_id': view_id,
            'view_type': 'form',
            'res_model': 'account.voucher',
            'type': 'ir.actions.act_window',
            'nodestroy': True,
            'target': 'new',
            'domain': '[]',
            'context': {
                'payment_expected_currency': inv.currency_id.id,
                'default_partner_id': self.pool.get('res.partner')._find_accounting_partner(inv.partner_id).id,
                'default_amount': inv.type in ('out_refund', 'in_refund') and -inv.residual or inv.residual,
                'default_reference': inv.name,
                'close_after_process': True,
                'invoice_type': inv.type,
                'invoice_id': inv.id,
                'default_journal_impot': inv.journal_id.id,
                'default_credit':inv.partner_id.payment_abonnement_due,
                'default_type': inv.type in ('out_invoice','out_refund') and 'receipt' or 'payment',
                'type': inv.type in ('out_invoice','out_refund') and 'receipt' or 'payment'
            }
        }

   
    _columns = {
        'credit':  fields.float('Credit', digits=(12, 0)),
        'compare_date': fields.function(calculate_date, type='date',string='Interval', store=True ),
        }
    
   
account_invoice()





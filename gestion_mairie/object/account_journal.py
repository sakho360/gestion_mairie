# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2008 PC Solutions (<http://pcsol.be>). All Rights Reserved
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import fields, osv

class account_journal(osv.osv):
    _inherit = 'account.journal'
    
    _columns = {
        'is_impot_journal': fields.boolean('Type impot'),
        'is_diouti': fields.boolean('Diouti'),
#         'property_payment_term': fields.property(
#             'account.payment.term',
#             type='many2one',
#             relation='account.payment.term',
#             string ='Customer Payment Term',
#             view_load=True,),
#             help="This payment term will be used instead of the default one for sale orders and customer invoices"
        'property_payment_term': fields.many2one('account.payment.term', string='Customer Payment Term'),
        'jour':fields.selection([
                    ('1', '1'),
                    ('2', '2'),
                    ('3', '3'),
                    ('4', '4'),
                     ('5', '5'),
                    ('6', '6'),
                     ('7', '7'),
                    ('8', '8'),
                    ('9', '9'),
                    ('10', '10'),
                      ('11', '11'),
                    ('12', '12'),
                    ('13', '13'),
                    ('14', '14'),
                     ('15', '15'),
                    ('16', '16'),
                     ('17', '17'),
                    ('18', '18'),
                    ('19', '19'),
                    ('20', '20'),
                     ('21', '21'),
                    ('22', '22'),
                    ('23', '23'),
                    ('24', '24'),
                     ('25', '25'),
                    ('26', '26'),
                     ('27', '27'),
                    ('28', '28'),
                    ('29', '29'),
                    ('30', '30'),
                    ('31', '31'),
                     ], 'Jour', select=True),
                'mois':fields.selection([
                    ('1', 'Janvier'),
                    ('2', 'Fevrier'),
                    ('3', 'Mars'),
                    ('4', 'Avril'),
                     ('5', 'Mai'),
                    ('6', 'Juin'),
                     ('7', 'Juillet'),
                    ('8', 'Aout'),
                    ('9', 'Septembre'),
                    ('10', 'Octobre'),
                      ('11', 'Novembre'),
                    ('12', 'Decembre')
                     ], 'Mois', select=True),   
    }

account_journal()

# 
# class wizard_multi_charts_accounts(osv.osv_memory):
#     _inherit = 'wizard.multi.charts.accounts'
#     
#     def _prepare_all_journals(self, cr, uid, chart_template_id, acc_template_ref, company_id, context=None):
#         def _get_analytic_journal(journal_type):
#             # Get the analytic journal
#             data = False
#             try:
#                 if journal_type in ('sale', 'sale_refund'):
#                     data = obj_data.get_object_reference(cr, uid, 'account', 'analytic_journal_sale')
#                 elif journal_type in ('purchase', 'purchase_refund'):
#                     data = obj_data.get_object_reference(cr, uid, 'account', 'exp')
#                 elif journal_type == 'general':
#                     pass
#             except ValueError:
#                 pass
#             return data and data[1] or False
# 
#         def _get_default_account(journal_type, type='debit'):
#             # Get the default accounts
#             default_account = False
#             if journal_type in ('sale', 'sale_refund'):
#                 default_account = acc_template_ref.get(template.property_account_income_categ.id)
#             elif journal_type in ('purchase', 'purchase_refund'):
#                 default_account = acc_template_ref.get(template.property_account_expense_categ.id)
#             elif journal_type in ('bank', 'cash'):
#                 default_account = acc_template_ref.get(template.bank_account_view_id.id)
#             elif journal_type == 'situation':
#                 if type == 'debit':
#                     default_account = acc_template_ref.get(template.property_account_expense_opening.id)
#                 else:
#                     default_account = acc_template_ref.get(template.property_account_income_opening.id)
#             return default_account

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:

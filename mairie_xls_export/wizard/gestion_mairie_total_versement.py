# -*- coding: utf-8 -*-

from openerp import tools
import openerp.addons.decimal_precision as dp
from openerp.osv import fields, osv

class gestion_mairie_total_versement(osv.osv):
    _name = "gestion.mairie.total.versement"
    _description = "gestion mairie total des versements"
    _auto = False
    _rec_name = 'date'
    
    _columns = {
        'credit':  fields.float('credit', readonly=True),
        'debit':   fields.float('debit', readonly=True),
        'date': fields.date('Date', readonly=True),
        'year': fields.char('Year', size=4, readonly=True),
        'day': fields.char('Day', size=128, readonly=True),
        'month': fields.selection([('01', 'January'), ('02', 'February'), ('03', 'March'), ('04', 'April'),
            ('05', 'May'), ('06', 'June'), ('07', 'July'), ('08', 'August'), ('09', 'September'),
            ('10', 'October'), ('11', 'November'), ('12', 'December')], 'Month', readonly=True),
        # 'period_id': fields.many2one('account.period', 'Force Period', readonly=True),
        'journal_impot': fields.many2one('account.journal', 'Journal', readonly=True),
        # 'user_currency_price_total': fields.function(_compute_amounts_in_user_currency, string="Total Without Tax", type='float', digits_compute=dp.get_precision('Account'), multi="_compute_amounts"),
        # 'price_average': fields.float('Average Price', readonly=True, group_operator="avg"),
        # 'user_currency_price_average': fields.function(_compute_amounts_in_user_currency, string="Average Price", type='float', digits_compute=dp.get_precision('Account'), multi="_compute_amounts"),
        # 'residual': fields.float('Total Residual', readonly=True),
        # 'user_currency_residual': fields.function(_compute_amounts_in_user_currency, string="Total Residual", type='float', digits_compute=dp.get_precision('Account'), multi="_compute_amounts"),
    }
    _order = 'date desc'
    
    def init(self, cr):
        # self._table = account_invoice_report
        tools.drop_view_if_exists(cr, self._table)
       # cr.execute("""CREATE SEQUENCE id_total_versement INCREMENT BY 1 START WITH 1""")
        #cr.execute("""SELECT nextval('id_total_versement')""")
        #(SELECT nextval('id_total_versement'))
        cr.execute("""CREATE OR REPLACE VIEW gestion_mairie_total_versement AS 
                    SELECT sub.id , sub.date, sub.year, sub.month, sub.day, sub.journal_impot, debit, credit
                     FROM 
                     (SELECT  min(a.id) as id,a.date AS date, 
                     to_char(a.date::timestamp with time zone, 'YYYY'::text) AS year,
                     to_char(a.date::timestamp with time zone, 'MM'::text) AS month,
                     to_char(a.date::timestamp with time zone, 'YYYY-MM-DD'::text) AS day,
                     a.journal_impot, 
                     SUM(a.amount) as debit , 0 as credit
                       FROM account_voucher a
                       left join account_journal aj on aj.id=a.journal_impot
                       where aj.code in('PT','TAXPU') and a.state='posted'
                       group by a.date, a.journal_impot
                    union
                    SELECT  min(a.id ) as id,a.date_versement AS date, 
                     to_char(a.date_versement::timestamp with time zone, 'YYYY'::text) AS year,
                     to_char(a.date_versement::timestamp with time zone, 'MM'::text) AS month,
                     to_char(a.date_versement::timestamp with time zone, 'YYYY-MM-DD'::text) AS day,
                     a.journal_impot, 0 as debit,
                     SUM(a.amount) as credit 
                       FROM account_voucher a
                       left join account_journal aj on aj.id=a.journal_impot
                       where aj.code in('ABO','DT') and verse=True and a.state='posted'
                       group by a.date_versement, a.journal_impot ) sub ;""")


# 
# SELECT  min(i.id) as id, i.create_date AS date, 
#                      to_char(i.create_date::timestamp with time zone, 'YYYY'::text) AS year,
#                      to_char(i.create_date::timestamp with time zone, 'MM'::text) AS month,
#                      to_char(i.create_date::timestamp with time zone, 'YYYY-MM-DD'::text) AS day,
#                      i.journal_id, 0 as debit,
#                      SUM(i.montant) as credit 
#                     FROM import_import i
#                        group by i.journal_id,day,i.create_date 
gestion_mairie_total_versement()

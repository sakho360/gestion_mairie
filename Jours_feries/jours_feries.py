# -*- coding: utf-8 -*-
from openerp.osv import osv, fields


class jours_feries(osv.osv):
    _name = 'jours.feries'
    _description = ' Les jours feries'    
    _columns = {
                'name':fields.char('Jours ferie', size=64, required=True),
                'jour':fields.selection([
                    ('1', '1'),
                    ('2', '2'),
                    ('3', '3'),
                    ('4', '4'),
                     ('5', '5'),
                    ('6', '6'),
                     ('7', '7'),
                    ('8', '8'),
                    ('9', '9'),
                    ('10', '10'),
                      ('11', '11'),
                    ('12', '12'),
                    ('13', '13'),
                    ('14', '14'),
                     ('15', '15'),
                    ('16', '16'),
                     ('17', '17'),
                    ('18', '18'),
                    ('19', '19'),
                    ('20', '20'),
                     ('21', '21'),
                    ('22', '22'),
                    ('23', '23'),
                    ('24', '24'),
                     ('25', '25'),
                    ('26', '26'),
                     ('27', '27'),
                    ('28', '28'),
                    ('29', '29'),
                    ('30', '30'),
                    ('31', '31'),
                     ], 'Jour', select=True, require=True),
                'mois':fields.selection([
                    ('1', 'Janvier'),
                    ('2', 'Fevrier'),
                    ('3', 'Mars'),
                    ('4', 'Avril'),
                     ('5', 'Mai'),
                    ('6', 'Juin'),
                     ('7', 'Juillet'),
                    ('8', 'Aout'),
                    ('9', 'Septembre'),
                    ('10', 'Octobre'),
                      ('11', 'Novembre'),
                    ('12', 'Decembre')
                     ], 'Mois', select=True, require=True),                
                }
    _defaults = {
                'jour': '1',
                'mois':  '1',
        }
jours_feries()

  